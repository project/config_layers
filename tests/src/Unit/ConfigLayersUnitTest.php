<?php

namespace Drupal\Tests\config_layers\Unit;

use Drupal\config_layers\Entity\ConfigLayer;
use Drupal\config_layers\ConfigLayerManager;
use Drupal\Core\Config\MemoryStorage;
use Drupal\Tests\UnitTestCase;

/**
 * Tests ConfigLayerManager.
 *
 * @coversDefaultClass \Drupal\config_layers\ConfigLayerManager
 * @group config_layers
 */
class ConfigLayersUnitTest extends UnitTestCase {

  /**
   * Test merging, deduplicating, replacing and normalizing config objects.
   *
   * @param array $source
   *   Source configuration.
   * @param array $target
   *   Target configuration.
   * @param array $expected
   *   Expected output.
   * @param string $update_mode
   *   Mode used for combining.
   *
   * @covers ::combine
   * @dataProvider layerCombineProvider
   */
  public function testLayerCombine(array $source, array $target, array $expected, $update_mode) {
    $source_storage = new MemoryStorage();
    $target_storage = new MemoryStorage();
    $source_storage->write('dummy_key', $source);
    $target_storage->write('dummy_key', $target);
    ConfigLayerManager::combine($source_storage, $target_storage, $update_mode);
    $output = $target_storage->read('dummy_key');
    static::assertSame($expected, $output);
  }

  /**
   * Provider of layer combine test cases.
   *
   * @return array
   *   Array of test cases.
   */
  public function layerCombineProvider() {
    return [
      // Simple merging.
      [
        ['x' => 0],
        ['y' => 1],
        ['x' => 0, 'y' => 1],
        ConfigLayer::UPDATE_MODE_MERGE,
      ],

      // Target is replaced by source.
      [
        ['x' => 0],
        ['y' => 1],
        ['x' => 0],
        ConfigLayer::UPDATE_MODE_REPLACE,
      ],

      // Source overwrites target.
      [
        ['x' => 0],
        ['x' => 1],
        ['x' => 0],
        ConfigLayer::UPDATE_MODE_MERGE,
      ],

      // Nested merge + normalization.
      [
        ['x' => ['y' => ['z' => 0]]],
        ['x' => ['y' => ['z' => 1, 'w' => 2]]],
        ['x' => ['y' => ['w' => 2, 'z' => 0]]],
        ConfigLayer::UPDATE_MODE_MERGE,
      ],

      // Merging numeric arrays. The array is replaced.
      [
        ['x' => [1, 3, 5]],
        ['x' => [2, 4, 6]],
        ['x' => [1, 3, 5]],
        ConfigLayer::UPDATE_MODE_MERGE,
      ],

      // Merging numeric keys.
      [
        ['x' => [1 => 1, 3 => 3, 5 => 5]],
        ['x' => [2 => 2, 4 => 4, 6 => 6]],
        ['x' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6]],
        ConfigLayer::UPDATE_MODE_MERGE,
      ],

      // Deduplicate identical objects.
      [
        ['x' => [1, 2, 3]],
        ['x' => [1, 2, 3]],
        FALSE,
        ConfigLayer::UPDATE_MODE_DEDUPLICATE,
      ],

      // Deduplicate source keys from target.
      [
        ['x' => ['y' => 0]],
        ['x' => ['y' => 0, 'z' => 1]],
        ['x' => ['z' => 1]],
        ConfigLayer::UPDATE_MODE_DEDUPLICATE,
      ],

      // Test normalization. See NormalizedStorage class.
      [
        ['a' => 'a', 'name' => 'name', 'uuid' => 12345],
        ['b' => 'b', 'status' => TRUE, 'settings' => []],
        [
          'uuid' => 12345,
          'status' => TRUE,
          'name' => 'name',
          'settings' => [],
          'a' => 'a',
          'b' => 'b',
        ],
        ConfigLayer::UPDATE_MODE_MERGE,
      ],
    ];
  }

}
