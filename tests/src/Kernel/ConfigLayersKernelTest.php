<?php

namespace Drupal\Tests\config_layers\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\config_layers\ConfigLayerManager;
use Drupal\config_layers\Entity\ConfigLayer;

/**
 * Test config layers.
 *
 * @group config_layers
 */
class ConfigLayersKernelTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'config_test',
    'config_layers',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig(['config_test']);
    $this->installConfig(['system']);
  }

  /**
   * Generate a specified number of layers.
   *
   * @param int $count
   *   Number of layers.
   *
   * @return array
   *   ConfigLayer entities.
   */
  protected function generateLayers($count) {
    $layers = [];
    for ($i = 0; $i < $count; $i++) {
      $options = [
        'id' => "layer_$i",
        'label' => '',
        'path' => '',
        'status' => TRUE,
        'reset' => FALSE,
        'weight' => $i,
        'source' => ConfigLayer::CONFIG_SOURCE_DEFAULT,
        'import_mode' => ConfigLayer::UPDATE_MODE_REPLACE,
        'export_mode' => ConfigLayer::UPDATE_MODE_REPLACE,
        'import_events' => [],
        'export_events' => [],
      ];
      $layers[$i] = ConfigLayer::create($options);
    }
    return $layers;
  }

  /**
   * Test generating and merging config layers.
   */
  public function testConfigLayers() {

    // Set the site name and slogan.
    $this->config('system.site')->set('name', 'name')->set('slogan', 'slogan')->save();

    $layers = $this->generateLayers(3);
    $layers[0]->getDatabaseStorage()->write('core.extension',
      ['module' => ['user' => 0, 'node' => 0]]
    );
    $layers[1]->getDatabaseStorage()->write('core.extension',
      ['module' => ['user' => 0, 'block' => 0]]
    );
    $layers[2]->getDatabaseStorage()->write('core.extension',
      ['module' => ['node' => 0, 'file' => 0]]
    );

    $merged = ConfigLayerManager::mergeLayers($layers);

    // Test merged result.
    $this->assertSame(
      ['module' => ['block' => 0, 'file' => 0, 'node' => 0, 'user' => 0]],
      $merged->read('core.extension')
    );

    // Deduplicated layer with index 1.
    $this->assertSame(
      ['module' => ['block' => 0]],
      $layers[1]->getDatabaseStorage()->read('core.extension')
    );

    // Deduplicated layer with index 2.
    $this->assertSame(
      ['module' => ['file' => 0]],
      $layers[2]->getDatabaseStorage()->read('core.extension')
    );

    $active_storage = \Drupal::service('config.storage');

    // Merge configuration into active configuration.
    // Note: we're bypassing config validation and module install hooks.
    ConfigLayerManager::combine($merged, $active_storage, ConfigLayer::UPDATE_MODE_MERGE);

    // Flush the cache.
    drupal_flush_all_caches();

    $moduleHandler = \Drupal::service('module_handler');

    // Assert modules enabled by config layers.
    $this->assert($moduleHandler->moduleExists('block'));
    $this->assert($moduleHandler->moduleExists('file'));
    $this->assert($moduleHandler->moduleExists('node'));
    $this->assert($moduleHandler->moduleExists('user'));

    // Assert modules installed by kernel test.
    $this->assert($moduleHandler->moduleExists('config_test'));
    $this->assert($moduleHandler->moduleExists('config_layers'));

    // Assert additional configuration.
    $this->assertSame('name', $this->config('system.site')->get('name'));
    $this->assertSame('slogan', $this->config('system.site')->get('slogan'));

    // Replace active configuration.
    ConfigLayerManager::combine($merged, $active_storage, ConfigLayer::UPDATE_MODE_REPLACE);

    // Flush the cache.
    drupal_flush_all_caches();

    // Assert modules enabled by config layers.
    $this->assert($moduleHandler->moduleExists('block'));
    $this->assert($moduleHandler->moduleExists('file'));
    $this->assert($moduleHandler->moduleExists('node'));
    $this->assert($moduleHandler->moduleExists('user'));

    // Assert additional configuration was removed.
    $this->assert(empty($this->config('system.site')->get('name')));
    $this->assert(empty($this->config('system.site')->get('slogan')));
  }

}
