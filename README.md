# Config Layers

## Introduction
Config Layers permits the import and export of configuration into multiple
layers (each layer has a database storage and a corresponding file storage).
Layers are merged and synchronized into the active configuration. The active
configuration itself can be imported into an arbitrary layer. Any layer will
retain only the configuration data that does not exist in the preceding
(lower weight) layers.


## Use cases
- Organize configuration into isolated layers (similarly to config splits).
- Multiple independent configuration workflows.
- Maintain changes in active configuration while
  updating feature and profile configurations.
- Allow specific active configuration objects to be reset by "override layers".


## How it works
Each config layer has its configuration storage on disk and its corresponding
storage in the database.
Configuration is synchronized between the disk and the database storages using
the `config-layers:import` and `config-layers:export` drush commands.

Layers are merged together to produce the final merged configuration.
The merged configuration can be imported into the active configuration using
the `config-layers:synchronize` drush command.

The merge process is performed both during import and synchronization.
During the merge an in-memory storage will retain the merged configuration.
Layers are ordered by weight and during the merge process layers are updated
so that only config entries that differ from any already merged configuration
is retained. Hence, each layer will contain a snapshot of the differences
from any lower weight layers.

Optionally, if the reset flag has been set, the merge process ignores already
merged configuration, using the layer configuration as-is.

The import command may specify the `--from-active` option. Configuration will
then be copied from the active configuration into a layer (and subsequently
deduplicated in the merge process).


## Usage

### Config layer entities
Config layers may be added from the admin pages
(Configuration -> Development -> Configuration layers)
or via drush commands.

A layer will have a designated folder specified by the `path` parameter that
will be imported from or exported to. The layer `weight` parameter defines in
what order a layer is merged.

If a layer has the reset flag set, the configuration objects within the layer
will ignore already merged configuration.

### Drush commands
Layers can be imported, exported and synchronized via drush commands:
* `config-layers:import` -- import config layer(s)
* `config-layers:export` -- export config layer(s)
* `config-layers:synchronize` -- synchronize merged config into active config
* `config-layers:create` -- create a new layer
* `config-layers:delete` -- delete the layer and its database storage
* `config-layers:get` -- similar to `config:get`
* `config-layers:set` -- similar to `config:set`
* `config-layers:status` -- similar to `config:status`


## Examples
Let's create three layers with the corresponding weights [-10, 0, 10]:

`drush config-layers:create --id profile --label Profile --path profiles/contrib/xyz/config --weight -10`

`drush config-layers:create --id active --label Active --path config/layers/active --weight 0`

`drush config-layers:create --id overrides --label Overrides --path config/layers/overrides --weight 10`

We'll start by importing our profile configuration into the database
(we assume that we have already some configuration in the file system):

`drush clim profile`

Note: It might be a good idea to separate profile configuration into different
"feature layers", similarly to the config split module.

Next, let's import our active configuration into the `active` layer:

`drush clim active --from-active`

The active configuration will be merged and any config objects are deduplicated
from the preceding profile layer.

By exporting the active layer, we will retain only the objects and the keys
that differ:

`drush clex active`

Finally we import the `overrides` layer:

`drush clim overrides`

This layer may override some of the configuration in either `active` or `profile`.

We import the merged (updted) configuration into the active configuration:

`drush clsync`

Similarly, when changes are made to `profile`, we may import and synchronize with
the active storage.


## Related modules
This module was heavily inspired by some other modules (in particular Config Split):

* [Config Split](https://www.drupal.org/project/config_split)
* [Config Overlay](https://www.drupal.org/project/config_overlay)
* [Config Dynamic Split](https://www.drupal.org/project/config_dynamic_split)
* [Config Distro](https://www.drupal.org/project/config_distro)
* [Config Merge](https://www.drupal.org/project/config_merge)
