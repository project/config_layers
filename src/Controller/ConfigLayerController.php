<?php

namespace Drupal\config_layers\Controller;

use Drupal\config_layers\Entity\ConfigLayerInterface;
use Drupal\Core\Controller\ControllerBase;

/**
 * Route controller for config layers.
 */
class ConfigLayerController extends ControllerBase {

  /**
   * Performs an operation on the config layer entity.
   *
   * @param \Drupal\config_layers\Entity\ConfigLayerInterface $config_layer
   *   The config layer entity.
   * @param string $op
   *   The operation to perform, usually 'enable' or 'disable'.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect back to the config layers settings page.
   */
  public function performOperation(ConfigLayerInterface $config_layer, $op) {
    $config_layer->$op()->save();

    $actions = [
      'enable' => 'enabled',
      'disable' => 'disabled',
      'import' => 'imported',
      'export' => 'exported',
    ];
    \Drupal::messenger()->addStatus($this->t('The %label layer has been %action.',
      ['%label' => $config_layer->label(), '%action' => $actions[$op]])
    );

    $url = $config_layer->toUrl('collection');
    return $this->redirect($url->getRouteName(), $url->getRouteParameters(), $url->getOptions());
  }

}
