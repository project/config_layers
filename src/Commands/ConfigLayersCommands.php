<?php

namespace Drupal\config_layers\Commands;

use Drupal\config_layers\ConfigLayerManager;
use Drupal\config_layers\Config\NormalizedStorage;
use Drupal\config_layers\Config\NormalizedStorageComparer;
use Drupal\config_layers\Entity\ConfigLayer;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\ImportStorageTransformer;
use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Config\StorageInterface;
use Drush\Commands\DrushCommands;
use Drush\Drupal\Commands\config\ConfigCommands;
use Drush\Drupal\Commands\config\ConfigImportCommands;
use Drush\Exceptions\UserAbortException;

/**
 * Drush commands for the Config Layers module.
 */
class ConfigLayersCommands extends DrushCommands {

  /**
   * Import action.
   */
  const IMPORT = 'import';

  /**
   * Export action.
   */
  const EXPORT = 'export';

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The active configuration storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $activeStorage;

  /**
   * The event dispatcher to notify the system that the config was imported.
   *
   * @var \Drupal\config_layers\ConfigLayerManager
   */
  protected $layerManager;

  /**
   * The service containing Drush commands for regular core config imports.
   *
   * @var \Drush\Drupal\Commands\config\ConfigImportCommands
   */
  protected $configImportCommands;

  /**
   * The import storage transformer.
   *
   * @var \Drush\Drupal\Commands\config\ImportStorageTransformer
   */
  protected $importStorageTransformer;

  /**
   * Constructs a new ConfigSyncCommands object.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The configuration manager.
   * @param \Drupal\config_layers\ConfigLayerManager $layerManager
   *   The config layers manager.
   * @param \Drupal\Core\Config\StorageInterface $activeStorage
   *   The active configuration storage.
   * @param \Drush\Drupal\Commands\config\ConfigImportCommands $configImportCommands
   *   Commands for importing configuration.
   * @param \Drupal\Core\Config\ImportStorageTransformer $importStorageTransformer
   *   The import storage transformer.
   */
  public function __construct(
    ConfigManagerInterface $config_manager,
    ConfigLayerManager $layerManager,
    StorageInterface $activeStorage,
    ConfigImportCommands $configImportCommands,
    ImportStorageTransformer $importStorageTransformer
  ) {
    parent::__construct();
    $this->configManager = $config_manager;
    $this->layerManager = $layerManager;
    $this->activeStorage = $activeStorage;
    $this->configImportCommands = $configImportCommands;
    $this->importStorageTransformer = $importStorageTransformer;
  }

  /**
   * Create config layer.
   *
   * @param array $options
   *   Additional options for the command.
   *
   * @command config-layers:create
   * @option string $id
   *   Layer id
   * @option label
   *   Layer label
   * @option path
   *   Path to config files.
   * @option reset
   *   If enabled this layer will overwrite configuration instead of merging.
   * @option weight
   *   Layer weight.
   * @option source
   *   Default source to use during import ('active' or 'default' = filesystem).
   */
  public function layerCreate(array $options = [
    'id' => '',
    'label' => '',
    'path' => '',
    'status' => TRUE,
    'reset' => FALSE,
    'weight' => 0,
    'source' => ConfigLayer::CONFIG_SOURCE_DEFAULT,
    'import_mode' => ConfigLayer::UPDATE_MODE_REPLACE,
    'export_mode' => ConfigLayer::UPDATE_MODE_REPLACE,
    'import_events' => [],
    'export_events' => [],
  ]) {
    $layer = ConfigLayer::create($options);
    $layer->save();
  }

  /**
   * Enable layer.
   *
   * @param string $layer_name
   *   The config layer name.
   *
   * @command config-layers:enable
   * @usage drush config-layers:enable active
   *   Enable the 'active' layer
   * @aliases clen,config-layers-enable
   */
  public function layerEnable($layer_name) {
    $layer = ConfigLayer::load($layer_name);
    if (isset($layer)) {
      $layer->enable();
      $layer->save();
    }
    else {
      $this->logger()->error("Layer named \"$layer_name\" has not been defined.");
    }
  }

  /**
   * Disable layer.
   *
   * @param string $layer_name
   *   The config layer name.
   *
   * @command config-layers:disable
   * @usage drush config-layers:disable active
   *   Disable the 'active' layer
   * @aliases cldis,config-layers-disable
   */
  public function layerDisable($layer_name) {
    $layer = ConfigLayer::load($layer_name);
    if (isset($layer)) {
      $layer->disable();
      $layer->save();
    }
    else {
      $this->logger()->error("Layer named \"$layer_name\" has not been defined.");
    }
  }

  /**
   * Display a layer config value, or a whole configuration object.
   *
   * @param string $layer_name
   *   The config layer name.
   * @param string $config_name
   *   The config object name, for example "system.site".
   * @param string $key
   *   The config key, for example "page.front". Optional.
   * @param array $options
   *   Additional options for the command.
   *
   * @command config-layers:get
   * @validate-config-name
   * @interact-config-name
   * @option source The config storage source to read. Additional labels may be defined in settings.php.
   * @option include-overridden Apply module and settings.php overrides to values.
   * @option merged Get the merged layer.
   * @usage drush config-layers:get active system.site
   *   Displays the system.site config from active layer.
   * @usage drush config-layers:get active system.site page.front
   *   Gets system.site:page.front value from active layer.
   * @aliases clget,config-layers-get
   */
  public function get($layer_name, $config_name, $key = '', array $options = [
    'format' => 'yaml',
    'source' => 'active',
    'include-overridden' => FALSE,
    'merged' => FALSE,
  ]) {
    $storage = $this->layerManager->getLayerStorage($layer_name, $options['merged']);
    $config_commands = new ConfigCommands($this->layerManager->getConfigFactory($storage), $storage);
    return $config_commands->get($config_name, $key, $options);
  }

  /**
   * Set layer config value directly. Does not perform a config import.
   *
   * @param string $layer_name
   *   The config layer name.
   * @param string $config_name
   *   The config object name, for example "system.site".
   * @param string $key
   *   The config key, for example "page.front".
   * @param string $value
   *   The value to assign to the config key. Use '-' to read from STDIN.
   * @param array $options
   *   Additional options for the command.
   *
   * @command config-layers:set
   * @validate-config-name
   * @option input-format Format to parse the object. Use "string" for string (default), and "yaml" for YAML.
   * @option value The value to assign to the config key (if any).
   * @hidden-options value
   * @usage drush config:set profile system.site page.front '/path/to/page'
   *   Sets the given path as value for the config item with key "page.front"
   *   of "system.site" config object in the config layer "profile".
   * @aliases clset,config-layers-set
   */
  public function set($layer_name, $config_name, $key, $value = NULL, array $options = [
    'input-format' => 'string',
    'value' => self::REQ,
  ]) {
    $storage = $this->layerManager->getLayerStorage($layer_name);
    $config_commands = new ConfigCommands($this->layerManager->getConfigFactory($storage), $storage);
    return $config_commands->set($config_name, $key, $value, $options);
  }

  /**
   * Delete a configuration key, a whole object, or a whole config layer.
   *
   * @param string $layer_name
   *   The config layer name.
   * @param string $config_name
   *   The config object name, for example "system.site" (Optional).
   * @param string $key
   *   A config key to clear, for example "page.front" (Optional).
   *
   * @command config-layers:delete
   *
   * @usage drush config_layers:delete profile
   *   Delete the the profile layer.
   * @usage drush config_layers:delete profile system.site
   *   Delete the the system.site config object from the profile layer.
   * @usage drush config_layers:delete profile system.site page.front
   *   Delete the 'page.front' key from the system.site object in the
   *   profile layer.
   * @aliases cldel,config-layers-delete
   */
  public function delete($layer_name, $config_name = '', $key = NULL) {
    if (empty($config_name)) {
      $this->layerManager->deleteLayer($layer_name);
    }
    else {
      $storage = $this->layerManager->getLayerStorage($layer_name);
      $config_commands = new ConfigCommands($this->layerManager->getConfigFactory($storage), $storage);
      return $config_commands->delete($config_name, $key);
    }
  }

  /**
   * Display status of configuration.
   *
   * @param string $layer_name
   *   The config layer name.
   * @param array $options
   *   Additional options for the command.
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   Table with configuration status.
   *
   * @command config-layers:status
   *
   * @option state  A comma-separated list of states to filter results.
   * @option prefix Prefix The config prefix. For example, "system". No prefix will return all names in the system.
   * @option string $label A config directory label (i.e. a key in \$config_directories array in settings.php).
   * @usage drush config:status profile
   *   Display configuration items that need to be synchronized
   *   in the profile layer.
   * @usage drush config:status --state=Identical profile
   *   Display configuration items that are in default state
   *   in the profile layer.
   * @usage drush config:status --state='Only in sync dir' --prefix=node.type. profile
   *   Display all content types that would be created in active storage
   *   on configuration import in the profile layer.
   * @usage drush config:status --state=Any --format=list profile
   *   List all config names in the profile layer.
   * @field-labels
   *   name: Name
   *   state: State
   * @default-fields name,state
   * @aliases clst,config-layer-status
   * @filter-default-field name
   */
  public function status($layer_name, array $options = [
    'state' => 'Only in DB,Only in sync dir,Different',
    'prefix' => self::REQ,
    'label' => self::REQ,
  ]) {
    // Use config layer directory unless label option is specified.
    if (!isset($options['label'])) {
      global $config_directories;
      $label = 'config_layer_' . $layer_name;
      $options['label'] = $label;
      $config_directories[$label] = ConfigLayer::load($layer_name)->getPath();
    }
    $storage = $this->layerManager->getLayerStorage($layer_name);
    $config_commands = new ConfigCommands($this->layerManager->getConfigFactory($storage), $storage);
    return $config_commands->status($options);
  }

  /**
   * Import configuration into layer.
   *
   * @param string $layers
   *   (Optional) Comma-separated list of layers to import configuration into
   *   (from filesystem to database). If omitted, import all active layers.
   * @param array $options
   *   Additional options for the command.
   *
   * @command config-layers:import
   *
   * @option source
   *   An arbitrary directory that holds the configuration files.
   * @option from
   *   active = import from active configuration.
   *   default = import from disk (default).
   * @option tag
   *   Import only layers from the specified tag(s) (comma separated list).
   * @option synchronize
   *   Synchronize into active configuration.
   * @option update-mode
   *   replace = delete target configuration and replace with source.
   *   copy = keep target configuration, replace only the objects from source.
   *   merge = merge source configuration on top of target.
   *   merge-target = merge target configuration on top of source, write to target.
   *   deduplicate = deduplicate target from duplicated configuration in source.
   *   deduplicate-source = deduplicate source from duplicated configuration in target, write to target.
   * @usage drush config-layers-import
   *   Import configuration into a configuration layer.
   * @aliases clim,config-layers-import
   */
  public function layersImport($layers = '', array $options = [
    'source' => '',
    'from' => '',
    'synchronize' => FALSE,
    'update-mode' => NULL,
    'tag' => '',
  ]) {

    if (empty($layers)) {
      $tags = empty($options['tag']) ? [] : explode(',', $options['tag']);
      $layers = $this->layerManager->getLayers($tags);
    }
    else {
      $layers = explode(',', $layers);
    }

    foreach ($layers as $layer_name) {
      $layer = ConfigLayer::load($layer_name);
      if (!isset($layer)) {
        $this->logger()->error("Layer named \"$layer_name\" has not been defined.");
      }

      $target_storage = $layer->getDatabaseStorage();

      if (!empty($options['from'])) {
        $layer->setSource($options['from']);
      }

      if (!empty($options['source'])) {
        $layer->setPath($options['source']);
      }

      // N: test against merged layer if importing from active configuration.
      $test_storage = $layer->getSource() == ConfigLayer::CONFIG_SOURCE_ACTIVE ?
        $this->layerManager->getMergedLayer($layer_name) : $target_storage;

      $source_storage = $layer->getSourceStorage();
      $update_mode = $options['update-mode'] ?? $layer->getImportMode();

      if ($this->confirmMessage($source_storage, $test_storage, self::IMPORT)) {
        ConfigLayerManager::combine($source_storage, $target_storage, $update_mode);
        $this->logger()->success(dt('The configuration was imported successfully into \'!layer\' configuration layer.', ['!layer' => $layer_name]));
      }
    }

    // Merge layers.
    $merged_storage = $this->layerManager->getMergedLayer();

    // Synchronize into active storage.
    if ($options['synchronize']) {
      $this->synchronizeActive($merged_storage, $this->activeStorage, self::IMPORT, TRUE);
    }
  }

  /**
   * Export config layer(s) to config files.
   *
   * @param string $layers
   *   Comma-separated list of layers to export.
   * @param array $options
   *   Additional options for the command.
   *
   * @command config-layers:export
   * @option destination
   *   An arbitrary directory that should receive the exported files.
   * @option tag
   *   Export only layers from the specified tag(s) (comma separated list).*
   * @option merge
   *   Export the merged result of all layers.
   * @option active-diff
   *   Export only the changes in active configuration.
   * @option update-mode
   *   replace = delete target configuration and replace with source.
   *   copy = keep target configuration, replace only the objects from source.
   *   merge = merge source configuration on top of target.
   *   merge-target = merge target configuration on top of source, write to target.
   *   deduplicate = deduplicate target from duplicated configuration in source.
   *   deduplicate-source = deduplicate source from duplicated configuration in target, write to target.
   * @usage drush config-layers-export
   *   Export configuration layers to disk.
   * @aliases clex,config-layers-export
   */
  public function layersExport($layers = '', array $options = [
    'destination' => NULL,
    'merge' => FALSE,
    'active-diff' => FALSE,
    'update-mode' => NULL,
    'tag' => '',
  ]) {

    if (empty($layers)) {
      $tags = empty($options['tag']) ? [] : explode(',', $options['tag']);
      $layers = $this->layerManager->getLayers($tags);
    }
    else {
      $layers = explode(',', $layers);
    }

    $source_is_layer = TRUE;
    if ($options['merge']) {
      $source_storage = $this->layerManager->getMergedLayer();
      $source_is_layer = FALSE;
    }
    elseif ($options['active-diff']) {
      $source_storage = $this->layerManager->getConfigDiff($this->activeStorage);
      $source_is_layer = FALSE;
    }

    foreach ($layers as $layer) {
      $entity = ConfigLayer::load($layer);
      if (!isset($entity)) {
        $this->logger()->error("Layer named \"$layer\" does not exist.");
      }

      $export_path = $options['destination'] ?? $entity->getPath();
      $file_storage = new FileStorage($export_path);

      $update_mode = $options['update-mode'] ?? $entity->getExportMode();

      if ($source_is_layer) {
        $source_storage = $entity->getDatabaseStorage();
      }

      if ($this->confirmMessage($source_storage, $file_storage, self::EXPORT)) {
        ConfigLayerManager::combine($source_storage, $file_storage, $update_mode);
        $this->logger()->success(dt('Configuration successfully exported to !target (update-mode=!mode).', [
          '!target' => realpath($export_path),
          '!mode' => $update_mode,
        ]));
      }

      if (!$source_is_layer && $options['destination']) {
        // Only export config once when 'destination' is provided.
        return;
      }
    }
  }

  /**
   * Synchronize layers into active configuration.
   *
   * @command config-layers:synchronize
   * @aliases clsync,config-layers-synchronize
   */
  public function synchronizeLayers() {
    $merged_storage = $this->layerManager->getMergedLayer();
    $this->synchronizeActive($merged_storage);
  }

  /**
   * Confirmation message. Shows differences in configuration.
   *
   * @param \Drupal\Core\Config\StorageInterface $source_storage
   *   Source storage.
   * @param \Drupal\Core\Config\StorageInterface $target_storage
   *   Target storage.
   * @param string $action
   *   One of self::IMPORT or self::EXPORT.
   */
  protected function confirmMessage(StorageInterface $source_storage, StorageInterface $target_storage, $action) {
    $storage_comparer = new NormalizedStorageComparer($source_storage, $target_storage, $this->configManager);
    if (!$storage_comparer->createChangelist()->hasChanges()) {
      $messages = [
        self::IMPORT => 'There are no changes to import.',
        self::EXPORT => 'There are no changes to export.',
      ];
      $this->logger()->notice(dt($messages[$action]));
      return FALSE;
    }

    $change_list = [];
    foreach ($storage_comparer->getAllCollectionNames() as $collection) {
      $change_list[$collection] = $storage_comparer->getChangelist(NULL, $collection);
    }
    $table = ConfigCommands::configChangesTable($change_list, $this->output());
    $table->render();

    $messages = [
      self::IMPORT => 'Import the listed configuration changes?',
      self::EXPORT => 'Export the listed configuration changes?',
    ];
    if ($this->io()->confirm(dt($messages[$action]))) {
      return TRUE;
    }
    else {
      throw new UserAbortException();
    }
  }

  /**
   * Synchronize storages.
   *
   * @param \Drupal\Core\Config\StorageInterface $source_storage
   *   Storage to import into the active configuration.
   */
  protected function synchronizeActive(StorageInterface $source_storage) {
    if ($this->confirmMessage($source_storage, $this->activeStorage, self::IMPORT)) {

      // N: ConfigImportCommands::doImport() will not invoke the import storage transformer.
      $source_storage = $this->importStorageTransformer->transform(new NormalizedStorage($source_storage));
      $target_storage = new NormalizedStorage($this->activeStorage);
      $storage_comparer = new StorageComparer($source_storage, $target_storage, $this->configManager);

      $storage_comparer->createChangelist();
      drush_op([$this->configImportCommands, 'doImport'], $storage_comparer);
    }
  }

}
