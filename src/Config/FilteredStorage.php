<?php

namespace Drupal\config_layers\Config;

use Drupal\Core\Config\StorageInterface;

/**
 * Represents a subset of configuration of the wrapped storage.
 */
class FilteredStorage extends StorageDecorator {

  /**
   * The names of the exposed configuration keys.
   *
   * @var array
   */
  private $keys;

  /**
   * Create a FilteredStorage with a decorated storage.
   *
   * @param \Drupal\Core\Config\StorageInterface $decorated
   *   The decorated storage.
   * @param array $keys
   *   Subset of keys that will be exposed from the underlying storage.
   */
  public function __construct(StorageInterface $decorated, array $keys) {
    $this->keys = $keys;
    parent::__construct($decorated);
  }

  /**
   * {@inheritdoc}
   */
  public function createCollection($collection) {
    return new FilteredStorage(parent::createCollection($collection), $this->keys);
  }

  /**
   * {@inheritdoc}
   */
  public function listAll($prefix = '') {
    $result = parent::listAll($prefix);
    return array_intersect($result, $this->keys);
  }

}
