<?php

namespace Drupal\config_layers\Config;

/**
 * Normalized storage. Sort config arrays by key.
 */
class NormalizedStorage extends StorageDecorator {

  /**
   * These config keys will always be sorted first (remaining keys are sorted alphabetically).
   */
  private const DEFAULT_COMMON_KEYS = [
    'uuid',
    'langcode',
    'status',
    'dependencies',
    'id',
    'name',
    'label',
    'type',
    'bundle',
    'description',
    'default',
    'weight',
    'module',
    'required',
    'settings',
    'third_party_settings',
  ];

  /**
   * {@inheritdoc}
   */
  public function read($name) {
    $config = $this->decorated->read($name);
    if (is_array($config)) {
      self::normalize($config);
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function readMultiple(array $names) {
    $list = $this->decorated->readMultiple($names);
    foreach ($list as &$item) {
      if (is_array($item)) {
        self::normalize($item);
      }
    }
    return $list;
  }

  /**
   * {@inheritdoc}
   */
  public function createCollection($collection) {
    return new NormalizedStorage($this->decorated->createCollection($collection));
  }

  /**
   * Common keys will have higher precedence than any other keys.
   *
   * @return array
   *   Each key of the returned array is mapped to a weight value.
   */
  public static function getCommonKeys() {
    return array_flip(self::DEFAULT_COMMON_KEYS);
  }

  /**
   * Normalize nested arrays, sorting by keys.
   *
   * @param array &$array
   *   Multidimensional array.
   */
  public static function normalize(array &$array) {
    $common_keys = static::getCommonKeys();
    return self::normalizeDeep($array, $common_keys);
  }

  /**
   * Normalize nested arrays, sorting by keys.
   *
   * @param array &$array
   *   Multidimensional array.
   * @param array &$common_keys
   *   Common keys to be sorted first (by value).
   */
  public static function normalizeDeep(array &$array, array &$common_keys = []) {
    $result = TRUE;
    foreach ($array as &$value) {
      if (is_array($value)) {
        $result &= self::normalizeDeep($value, $common_keys);
      }
    }
    return uksort($array, function ($a, $b) use (&$common_keys) {
      return self::normalizedCompare($a, $b, $common_keys);
    }) && $result;
  }

  /**
   * First order by standard keys, second do normal string comparison.
   *
   * @param string $a
   *   The first string.
   * @param string $b
   *   The second string.
   * @param array &$common_keys
   *   Common keys to be sorted first (by value).
   */
  public static function normalizedCompare($a, $b, array &$common_keys) {
    if (isset($common_keys[$a])) {
      if (isset($common_keys[$b])) {
        return $common_keys[$a] <=> $common_keys[$b];
      }
      else {
        return -1;
      }
    }
    elseif (isset($common_keys[$b])) {
      return 1;
    }
    else {
      return strnatcmp($a, $b);
    }
  }

}
