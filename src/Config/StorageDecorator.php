<?php

namespace Drupal\config_layers\Config;

use Drupal\Core\Config\StorageInterface;

/**
 * Class StorageDecorator.
 *
 * Decorator class for another storage.
 */
class StorageDecorator implements StorageInterface {

  /**
   * The decorated storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $decorated;

  /**
   * Create a StorageDecorator with a decorated storage.
   *
   * @param \Drupal\Core\Config\StorageInterface $decorated
   *   The decorated storage.
   */
  public function __construct(StorageInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function exists($name) {
    return $this->decorated->exists($name);
  }

  /**
   * {@inheritdoc}
   */
  public function read($name) {
    return $this->decorated->read($name);
  }

  /**
   * {@inheritdoc}
   */
  public function readMultiple(array $names) {
    return $this->decorated->readMultiple($names);
  }

  /**
   * {@inheritdoc}
   */
  public function write($name, array $data) {
    return $this->decorated->write($name, $data);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($name) {
    return $this->decorated->delete($name);
  }

  /**
   * {@inheritdoc}
   */
  public function rename($name, $new_name) {
    return $this->decorated->rename($name, $new_name);
  }

  /**
   * {@inheritdoc}
   */
  public function encode($data) {
    return $this->decorated->encode($data);
  }

  /**
   * {@inheritdoc}
   */
  public function decode($raw) {
    return $this->decorated->decode($raw);
  }

  /**
   * {@inheritdoc}
   */
  public function listAll($prefix = '') {
    return $this->decorated->listAll($prefix);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll($prefix = '') {
    return $this->decorated->deleteAll($prefix);
  }

  /**
   * {@inheritdoc}
   */
  public function createCollection($collection) {
    return $this->decorated->createCollection($collection);
  }

  /**
   * {@inheritdoc}
   */
  public function getAllCollectionNames() {
    return $this->decorated->getAllCollectionNames();
  }

  /**
   * {@inheritdoc}
   */
  public function getCollectionName() {
    return $this->decorated->getCollectionName();
  }

}
