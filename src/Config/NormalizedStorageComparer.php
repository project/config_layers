<?php

namespace Drupal\config_layers\Config;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Config\StorageInterface;

/**
 * Defines a normalized config storage comparer.
 */
class NormalizedStorageComparer extends StorageComparer {

  /**
   * {@inheritdoc}
   */
  public function __construct(StorageInterface $source_storage, StorageInterface $target_storage, ConfigManagerInterface $config_manager = NULL) {
    $normalized_source_storage = new NormalizedStorage($source_storage);
    $normalized_target_storage = new NormalizedStorage($target_storage);
    parent::__construct($normalized_source_storage, $normalized_target_storage, $config_manager);
  }

}
