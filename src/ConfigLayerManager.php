<?php

namespace Drupal\config_layers;

use Drupal\Component\Utility\NestedArray;
use Drupal\config_layers\Config\FilteredStorage;
use Drupal\config_layers\Config\NormalizedStorage;
use Drupal\config_layers\Entity\ConfigLayer;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\MemoryStorage;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drush\Drupal\Commands\config\ConfigCommands;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Manage configuration layers.
 */
class ConfigLayerManager {

  /**
   * The config layer entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * The typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher definition.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * Constructs a new ConfigLayerManager object.
   *
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config
   *   The typed configuration manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher to tell other modules about the successful import.
   */
  public function __construct(
    TypedConfigManagerInterface $typed_config,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->typedConfigManager = $typed_config;
    $this->eventDispatcher = $event_dispatcher;
    $this->entityStorage = \Drupal::service('entity_type.manager')->getStorage('config_layer');
  }

  /**
   * Get ids of all active layers.
   *
   * @param array $tags
   *   Tags to filter by.
   *
   * @return array
   *   Layer ids of all active layers, sorted by weight.
   */
  public function getLayers(array $tags = []) {
    $layers = $this->entityStorage->getQuery()
      ->condition('status', 1)
      ->sort('weight')
      ->execute();

    return static::filterByTags($layers, $tags);
  }

  /**
   * Get layer by id.
   *
   * @return \Drupal\config_layers\Entity\ConfigLayer
   *   The layer entity.
   */
  public function getLayer($layer_id) {
    $layer = ConfigLayer::load($layer_id);
    if (isset($layer)) {
      return $layer;
    }
    else {
      throw new \Exception(dt('Configuration layer !id not found.', ['!id' => $layer_id]));
    }
  }

  /**
   * Delete layer by id.
   *
   * @param string $layer_id
   *   The layer id.
   */
  public function deleteLayer($layer_id) {
    $layer = ConfigLayer::load($layer_id);
    if (isset($layer)) {
      $layer->delete();
    }
    else {
      throw new \Exception(dt('Configuration layer !id not found.', ['!id' => $layer_id]));
    }
  }

  /**
   * Get a config factory wrapping the storage.
   *
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   The storage parameterfor the config factory.
   *
   * @return \Drupal\Core\Config\ConfigFactory
   *   Config factory instance.
   */
  public function getConfigFactory(StorageInterface $storage) {
    return new ConfigFactory($storage, $this->eventDispatcher, $this->typedConfigManager);
  }

  /**
   * Get the database storage associated with layer.
   *
   * @param string $layer_id
   *   The layer id.
   * @param bool $merged
   *   (optional) If set, return the merged storage
   *   of all layers up until this one.
   *
   * @return \Drupal\Core\Config\StorageInterface
   *   The storage associated with layer.
   */
  public function getLayerStorage($layer_id, $merged = FALSE) {
    if ($merged) {
      return $this->getMergedLayer($layer_id);
    }
    else {
      $layer = ConfigLayer::load($layer_id);
      if (isset($layer)) {
        return $layer->getDatabaseStorage();
      }
      else {
        throw new \Exception(dt('Configuration layer !id not found.', ['!id' => $layer_id]));
      }
    }
  }

  /**
   * Process single layer.
   *
   * @param \Drupal\Core\Config\StorageInterface $merged
   *   Merged configuration. Will be updated with additional
   *   configuration provided by $layer.
   * @param \Drupal\Core\Config\StorageInterface $layer
   *   Config layer. Remove config entries (keys) with no difference.
   * @param bool $reset
   *   (Optional) Ignore merged configuration and overwrite instead of merge.
   */
  protected static function processLayer(StorageInterface $merged, StorageInterface $layer, $reset = FALSE) {

    foreach ($layer->listAll() as $key) {
      if (!$reset && $merged->exists($key)) {
        $merged_val = $merged->read($key);
        $layer_val = $layer->read($key);

        // Get the deduplicated result (remove identical entries from layer config).
        $config = static::nestedArrayDedupeDeep($layer_val, $merged_val);

        // All entries match => delete key from layer storage.
        if (empty($config)) {
          $layer->delete($key);
        }
        else {
          // Some entries match => update layer and merge in differences.
          $layer->write($key, $config);
          $merged_config = NestedArray::mergeDeepArray([$merged_val, $layer_val], TRUE);
          NormalizedStorage::normalize($merged_config);
          $merged->write($key, $merged_config);
        }
      }
      else {
        // New key, append to merged storage:
        $config = $layer->read($key);
        NormalizedStorage::normalize($config);
        $merged->write($key, $config);
      }
    }

  }

  /**
   * Remove items from an array if they are identical.
   *
   * @param array $array1
   *   The array from which duplicates should be removed.
   * @param array $array2
   *   The source array for comparison.
   *
   * @return array
   *   The resulting deduped array.
   */
  public static function nestedArrayDedupeDeep(array $array1, array $array2) {
    $result = $array1;
    foreach ($array1 as $key => $val) {
      if (is_array($val)) {
        if (isset($array2[$key])) {
          $result[$key] = static::nestedArrayDedupeDeep($array1[$key], $array2[$key]);
        }
        else {
          $result[$key] = $array1[$key];
        }
      }
      elseif (array_key_exists($key, $array2) && $val === $array2[$key]) {
        unset($result[$key]);
      }
    }

    // Remove empty array keys.
    return array_filter($result, function ($value) {
      return !(is_array($value) && count($value) === 0);
    });
  }

  /**
   * Merge collections.
   *
   * @see ConfigLayerManager::processLayer()
   */
  protected static function mergeCollections(StorageInterface $merged, StorageInterface $layer, $reset = FALSE) {
    static::processLayer($merged, $layer, $reset);

    foreach ($layer->getAllCollectionNames() as $collection_name) {
      $merged_collection = $merged->createCollection($collection_name);
      $layer_collection = $layer->createCollection($collection_name);
      static::processLayer($merged_collection, $layer_collection, $reset);
    }
  }

  /**
   * Merge layers.
   *
   * Merge layers into temporary storage + remove config keys with no differences
   * from existing layers.
   *
   * @param array $layers
   *   Layer entities to merge.
   * @param array $keys
   *   (Optional) Subset of configuration keys to include in the merge operation.
   *
   * @return \Drupal\Core\Config\MemoryStorage
   *   Merged layer storage.
   */
  public static function mergeLayers(array $layers, array $keys = NULL) {

    if (empty($layers)) {
      return NULL;
    }

    $first_layer = array_shift($layers);

    $merged_storage = new MemoryStorage();
    ConfigCommands::copyConfig($first_layer->getDatabaseStorage(), $merged_storage);

    foreach ($layers as $layer) {
      $layer_storage = $layer->getDatabaseStorage();
      if (isset($keys)) {
        $layer_storage = new FilteredStorage($layer_storage, $keys);
      }
      static::mergeCollections($merged_storage, $layer_storage, $layer->getReset());
    }

    return $merged_storage;
  }

  /**
   * Get the merged layer storage.
   *
   * @param string $layer
   *   (optional) The final layer to merge. If omitted, all layers will be merged.
   *
   * @return \Drupal\Core\Config\MemoryStorage
   *   Merged layer storage.
   */
  public function getMergedLayer($layer = '') {
    $ids = array_values($this->getLayers());
    if ($offset = array_search($layer, $ids)) {
      $ids = array_slice($ids, 0, $offset + 1);
    }
    $layers = ConfigLayer::loadMultiple($ids);
    return $this->mergeLayers($layers);
  }

  /**
   * Get the configuration differences for a storage compared to merged configuration.
   *
   * @param \Drupal\Core\Config\StorageInterface $source_storage
   *   Configuration storage to deduplicate from merged configuration.
   * @param bool $reset
   *   (Optional) Ignore merged configuration and overwrite instead of merge.
   *
   * @return \Drupal\Core\Config\StorageInterface
   *   The result will include differences from currently merged configuration layers.
   */
  public function getConfigDiff(StorageInterface $source_storage, $reset = FALSE) {
    $diff_storage = new MemoryStorage();
    ConfigCommands::copyConfig($source_storage, $diff_storage);
    $merged_storage = $this->getMergedLayer();
    static::mergeCollections($merged_storage, $diff_storage, $reset);
    return $diff_storage;
  }

  /**
   * Delete storage and all its collections.
   *
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   Storage to be cleared.
   */
  public static function clearStorage(StorageInterface $storage) {
    $storage->deleteAll();
    foreach ($storage->getAllCollectionNames() as $collection_name) {
      $collection = $storage->createCollection($collection_name);
      $collection->deleteAll();
    }
  }

  /**
   * Combine source and target storage. Write result to target.
   *
   * @param \Drupal\Core\Config\StorageInterface $source_storage
   *   The source storage.
   * @param \Drupal\Core\Config\StorageInterface $target_storage
   *   The target storage.
   * @param string $update_mode
   *   The update mode determines how layers are combined.
   */
  public static function combine(StorageInterface $source_storage, StorageInterface $target_storage, $update_mode) {

    $tmp_storage = new MemoryStorage();
    ConfigCommands::copyConfig($source_storage, $tmp_storage);

    $tmp_storage = new NormalizedStorage($tmp_storage);
    $target_storage = new NormalizedStorage($target_storage);

    switch ($update_mode) {
      case ConfigLayer::UPDATE_MODE_COPY:
        ConfigCommands::copyConfig($tmp_storage, $target_storage);
        break;

      case ConfigLayer::UPDATE_MODE_REPLACE:
        static::clearStorage($target_storage);
        ConfigCommands::copyConfig($tmp_storage, $target_storage);
        break;

      case ConfigLayer::UPDATE_MODE_MERGE:
        // Merge source into target.
        static::mergeCollections($target_storage, $tmp_storage, FALSE);
        break;

      case ConfigLayer::UPDATE_MODE_MERGE_TARGET:
        // Merge target into source.
        static::mergeCollections($tmp_storage, $target_storage, FALSE);
        ConfigCommands::copyConfig($tmp_storage, $target_storage);
        break;

      case ConfigLayer::UPDATE_MODE_DEDUPLICATE:
        // Deduplicate target from source.
        static::mergeCollections($tmp_storage, $target_storage, FALSE);
        break;

      case ConfigLayer::UPDATE_MODE_DEDUPLICATE_SOURCE:
        // Deduplicate source from target.
        static::mergeCollections($target_storage, $tmp_storage, FALSE);
        static::clearStorage($target_storage);
        ConfigCommands::copyConfig($tmp_storage, $target_storage);
        break;
    }
  }

  /**
   * Synchronize storages.
   *
   * @param array $layers
   *   Config layer entities.
   * @param array $tags
   *   Tags to filter by.
   *
   * @return array
   *   Layer entities.
   */
  protected static function filterByTags(array $layers, array $tags) {
    if (empty($tags)) {
      return $layers;
    }
    $result = [];
    foreach ($layers as $layer) {
      if (!empty(array_intersect($layer->getTags(), $tags))) {
        $result[] = $layer;
      }
    }
    return $result;
  }

}
