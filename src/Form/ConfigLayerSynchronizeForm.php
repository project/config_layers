<?php

namespace Drupal\config_layers\Form;

use Drupal\config\Form\ConfigSync;
use Drupal\config_layers\Config\NormalizedStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Construct the storage changes in a configuration synchronization form.
 *
 * @internal
 */
class ConfigLayerSynchronizeForm extends ConfigSync {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      new NormalizedStorage($container->get('config_layers.manager')->getMergedLayer()),
      new NormalizedStorage($container->get('config.storage')),
      $container->get('config.storage.snapshot'),
      $container->get('lock.persistent'),
      $container->get('event_dispatcher'),
      $container->get('config.manager'),
      $container->get('config.typed'),
      $container->get('module_handler'),
      $container->get('module_installer'),
      $container->get('theme_handler'),
      $container->get('renderer'),
      $container->get('extension.list.module'),
      $container->get('config.import_transformer')
    );
  }

}
