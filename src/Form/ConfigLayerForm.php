<?php

namespace Drupal\config_layers\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The entity form.
 */
class ConfigLayerForm extends EntityForm {

  /**
   * The drupal state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal\Core\Extension\ThemeHandler definition.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The drupal state.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler.
   */
  public function __construct(StateInterface $state, ThemeHandlerInterface $themeHandler) {
    $this->state = $state;
    $this->themeHandler = $themeHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\config_layers\Entity\ConfigLayerInterface $config */
    $config = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $config->label(),
      '#description' => $this->t("Label for the Config Layer setting."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $config->id(),
      '#machine_name' => [
        'exists' => '\Drupal\config_layers\Entity\ConfigLayer::load',
      ],
    ];

    $form['static_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Static Settings'),
      '#description' => $this->t("These settings need a cache clear when overridden in settings.php and the layer needs to be single imported before the config import for new values to take effect."),
    ];
    $form['static_fieldset']['tags'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Layer tags'),
      '#default_value' => implode(', ', $config->get('tags')),
    ];
    $form['static_fieldset']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Describe this config layer setting. The text will be displayed on the <em>Config Layer setting</em> list page.'),
      '#default_value' => $config->get('description'),
    ];
    $form['static_fieldset']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#description' => $this->t('The directory, relative to the Drupal root, to which to save the layer config.'),
      '#default_value' => $config->get('path'),
    ];
    $form['static_fieldset']['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Source'),
      '#description' => $this->t('The source storage to import configuration from.'),
      '#options' => [
        'default' => $this->t('Configuration on disk (default)'),
        'active' => $this->t('Active configuration'),
      ],
      '#default_value' => $config->get('source'),
    ];
    $form['static_fieldset']['weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight'),
      '#description' => $this->t('The weight to order the layers.'),
      '#default_value' => $config->get('weight'),
    ];
    $form['static_fieldset']['reset'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reset'),
      '#description' => $this->t('Reset configuration from previous layers (configuration will be overwritten instead of merged).'),
      '#default_value' => ($config->get('reset') ? TRUE : FALSE),
    ];
    $form['static_fieldset']['import_events'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Import events'),
      '#description' => $this->t('Trigger layer import for specific events (e.g. config.save,config.delete,config.rename).'),
      '#default_value' => implode(', ', $config->get('import_events')),
    ];
    $form['static_fieldset']['export_events'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Export events'),
      '#description' => $this->t('Trigger layer export for specific events (e.g. config.save,config.delete,config.rename).'),
      '#default_value' => implode(', ', $config->get('export_events')),
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('If enabled this layer will be used in import, export and synchronization.'),
      '#default_value' => ($config->get('status') ? TRUE : FALSE),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $path = $form_state->getValue('path');
    if (static::isConflicting($path)) {
      $form_state->setErrorByName('path', $this->t('The layer folder can not be in the sync folder.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $config_layer = $this->entity;
    $config_layer->set('tags', explode(',', str_replace(' ', '', $form_state->getValue('tags'))));
    $config_layer->set('import_events', explode(',', str_replace(' ', '', $form_state->getValue('import_events'))));
    $config_layer->set('export_events', explode(',', str_replace(' ', '', $form_state->getValue('export_events'))));
    $status = $config_layer->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Config Layer setting.', [
          '%label' => $config_layer->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Config Layer setting.', [
          '%label' => $config_layer->label(),
        ]));
    }
    $path = $form_state->getValue('path');
    if (!empty($path) && !file_exists($path)) {
      $this->messenger()->addWarning(
        $this->t('The storage path "%path" for %label Config Layer setting does not exist. Make sure it exists and is writable.',
          [
            '%label' => $config_layer->label(),
            '%path' => $path,
          ]
        ));
    }
    $form_state->setRedirectUrl($config_layer->toUrl('collection'));
  }

  /**
   * Check whether the path name conflicts with the default sync directory.
   *
   * @param string $path
   *   The config layer folder name to check.
   *
   * @return bool
   *   True if the path is inside the sync directory.
   */
  protected static function isConflicting($path) {
    return strpos(rtrim($path, '/') . '/', rtrim(Settings::get('config_sync_directory'), '/') . '/') !== FALSE;
  }

}
