<?php

namespace Drupal\config_layers\EventSubscriber;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\StorageTransformEvent;
use Drupal\config_layers\ConfigLayerManager;
use Drupal\config_layers\Entity\ConfigLayer;
use Drupal\config_layers\Config\FilteredStorage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Config subscriber to allow automatic import of active configuration into layer.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  const IMPORT_PRIORITY = 10;
  const EXPORT_PRIORITY = -10;

  /**
   * The Drupal Kernel.
   *
   * @var \Drupal\Core\ConfigLayerManager
   */
  protected $layerManager;

  /**
   * ConfigSubscriber constructor.
   *
   * @param \Drupal\config_layers\ConfigLayerManager $layerManager
   *   The layer manager.
   */
  public function __construct(ConfigLayerManager $layerManager) {
    $this->layerManager = $layerManager;
  }

  /**
   * Import new configuration into layers listening to events.
   *
   * @param \Drupal\Component\EventDispatcher\Event $event
   *   The event to process.
   * @param string $eventName
   *   Name of the event.
   */
  public function importConfig(Event $event, $eventName) {
    $layer_ids = $this->layerManager->getLayers();
    $layers = ConfigLayer::loadMultiple($layer_ids);

    if ($event instanceof ConfigCrudEvent) {
      $saved_config = $event->getConfig();
      $config_name = $saved_config->getName();
      $keys = [$config_name];

      foreach ($layers as $layer) {
        if (in_array($eventName, $layer->getImportEvents())) {
          switch ($eventName) {
            case ConfigEvents::SAVE:
              // Optimization: import only the saved key.
              $source = new FilteredStorage($layer->getSourceStorage(), $keys);
              $layer->import($source, ConfigLayer::UPDATE_MODE_COPY);
              break;

            case ConfigEvents::DELETE:
              // Optimization: delete only the key.
              $layer->getDatabaseStorage()->delete($config_name);
              break;

            default:
              $keys = NULL;
              $layer->import();
          }
        }
      }
    }
    else {
      $keys = NULL;
      foreach ($layers as $layer) {
        if (in_array($eventName, $layer->getImportEvents())) {
          $layer->import();
        }
      }
    }

    $this->layerManager->mergeLayers($layers, $keys);
  }

  /**
   * Export configuration to layers listening to events.
   *
   * @param \Drupal\Component\EventDispatcher\Event $event
   *   The event to process.
   * @param string $eventName
   *   Name of the event.
   */
  public function exportConfig(Event $event, $eventName) {
    $layer_ids = $this->layerManager->getLayers();
    $layers = ConfigLayer::loadMultiple($layer_ids);
    foreach ($layers as $layer) {
      if (in_array($eventName, $layer->getExportEvents())) {
        $layer->export();
      }
    }
  }

  /**
   * React to the import transformation.
   *
   * @param \Drupal\Core\Config\StorageTransformEvent $event
   *   The transformation event.
   * @param string $eventName
   *   Name of the event.
   */
  public function configImportTransform(StorageTransformEvent $event, $eventName) {
    $storage = $event->getStorage();
    if ($storage->exists('core.extension')) {
      $core_extension = $storage->read('core.extension');
      $core_extension['module'] = module_config_sort($core_extension['module']);
      $storage->write('core.extension', $core_extension);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    $events = [];
    if (\Drupal::hasContainer()) {
      try {
        $layers = \Drupal::entityTypeManager()->getStorage('config_layer')->loadMultiple();
        $import_events = [];
        $export_events = [];

        /** @var \Drupal\config_layers\Entity\ConfigLayer $layer */
        foreach ($layers as $layer) {
          if ($layer->status()) {
            $import_events += $layer->getImportEvents();
            $export_events += $layer->getExportEvents();
          }
        }

        foreach (array_unique($import_events) as $event) {
          $events[$event][] = ['importConfig', static::IMPORT_PRIORITY];
        }
        foreach (array_unique($export_events) as $event) {
          $events[$event][] = ['exportConfig', static::EXPORT_PRIORITY];
        }

        // Special handling of core.extension configuration.
        $events['config.transform.import'][] = ['configImportTransform', 0];
      }
      catch (\Exception $e) {
      }
    }
    return $events;
  }

}
