<?php

namespace Drupal\config_layers\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Provides an interface for defining Configuration Layer setting entities.
 */
interface ConfigLayerInterface extends ConfigEntityInterface {

  /**
   * Layer description.
   *
   * @return string
   *   Short description.
   */
  public function getDescription();

  /**
   * Sets the description to the given value.
   *
   * @param string $description
   *   The layer description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Path to the file storage.
   *
   * @return string
   *   The directory path.
   */
  public function getPath();

  /**
   * Sets the file storage path to the given value.
   *
   * @param string $path
   *   The layer path on disk.
   *
   * @return $this
   */
  public function setPath($path);

  /**
   * Layer weight. Used to order layers during merge.
   *
   * @return int
   *   The layer weight.
   */
  public function getWeight();

  /**
   * Sets the layer weight to the given value.
   *
   * @param int $weight
   *   The layer weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * If set, this layer will ignore merged configuration.
   *
   * @return bool
   *   The reset flag.
   */
  public function getReset();

  /**
   * Sets the reset parameter to the given value.
   *
   * @param bool $reset
   *   The reset flag.
   *
   * @return $this
   */
  public function setReset($reset);

  /**
   * Get layer tags.
   *
   * @return array
   *   The layer tags.
   */
  public function getTags();

  /**
   * Sets the layer tags to the given value.
   *
   * @param array $tags
   *   The layer tags.
   *
   * @return $this
   */
  public function setTags(array $tags);

  /**
   * The layer export mode.
   *
   * @return string
   *   Reset configuration parameter.
   */
  public function getExportMode();

  /**
   * The layer import mode.
   *
   * @return string
   *   Reset configuration parameter.
   */
  public function getImportMode();

  /**
   * Get the database storage associated with this layer.
   *
   * @return \Drupal\Core\Config\DatabaseStorage
   *   The database storage.
   */
  public function getDatabaseStorage();

  /**
   * Get the file storage associated with this layer.
   *
   * @return \Drupal\Core\Config\FileStorage
   *   The file storage.
   */
  public function getFileStorage();

  /**
   * Import the current layer.
   *
   * @param \Drupal\Core\Config\StorageInterface $source_storage
   *   The storage source storage (optional).
   * @param string $import_mode
   *   The import mode (optional).
   *
   * @return $this
   */
  public function import(StorageInterface $source_storage = NULL, $import_mode = NULL);

  /**
   * Export the current layer.
   *
   * @param \Drupal\Core\Config\StorageInterface $target_storage
   *   The target source storage (optional).
   * @param string $export_mode
   *   The export mode (optional).
   *
   * @return $this
   */
  public function export(StorageInterface $target_storage = NULL, $export_mode = NULL);

}
