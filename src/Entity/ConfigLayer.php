<?php

namespace Drupal\config_layers\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\DatabaseStorage;
use Drupal\Core\Config\FileStorage;
use Drupal\config_layers\ConfigLayerManager;

/**
 * Defines the Configuration Layer setting entity.
 *
 * @ConfigEntityType(
 *   id = "config_layer",
 *   label = @Translation("Configuration Layer setting"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\config_layers\ConfigLayerListBuilder",
 *     "form" = {
 *       "add" = "Drupal\config_layers\Form\ConfigLayerForm",
 *       "edit" = "Drupal\config_layers\Form\ConfigLayerForm",
 *       "delete" = "Drupal\config_layers\Form\ConfigLayerDeleteForm",
 *     },
 *   },
 *   config_prefix = "layer",
 *   admin_permission = "administer configuration layers",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/development/configuration/config-layers/{config_layer}",
 *     "add-form" = "/admin/config/development/configuration/config-layers/add",
 *     "edit-form" = "/admin/config/development/configuration/config-layers/{config_layer}/edit",
 *     "delete-form" = "/admin/config/development/configuration/config-layers/{config_layer}/delete",
 *     "enable" = "/admin/config/development/configuration/config-layers/{config_layer}/enable",
 *     "disable" = "/admin/config/development/configuration/config-layers/{config_layer}/disable",
 *     "collection" = "/admin/config/development/configuration/config-layers",
 *     "import" = "/admin/config/development/configuration/config-layers/{config_layer}/import",
 *     "export" = "/admin/config/development/configuration/config-layers/{config_layer}/export",
 *     "synchronize" = "/admin/config/development/configuration/config-layers/synchronize"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "tags",
 *     "path",
 *     "weight",
 *     "status",
 *     "reset",
 *     "source",
 *     "import_mode",
 *     "export_mode",
 *     "import_events",
 *     "export_events"
 *   }
 * )
 */
class ConfigLayer extends ConfigEntityBase implements ConfigLayerInterface {

  /**
   * Default layer storage.
   *
   * @var string
   */
  public const CONFIG_SOURCE_DEFAULT = 'default';

  /**
   * Active storage.
   *
   * @var string
   */
  public const CONFIG_SOURCE_ACTIVE = 'active';

  /**
   * Copy source on top of target. Delete remaining objects.
   */
  const UPDATE_MODE_REPLACE = 'replace';

  /**
   * Copy source on top of target. Preserve remaining objects.
   */
  const UPDATE_MODE_COPY = 'copy';

  /**
   * Merge source on top of target.
   */
  const UPDATE_MODE_MERGE = 'merge';

  /**
   * Merge target on top of source.
   */
  const UPDATE_MODE_MERGE_TARGET = 'merge-target';

  /**
   * Deduplicate target from already existing values in source.
   */
  const UPDATE_MODE_DEDUPLICATE = 'deduplicate';

  /**
   * Deduplicate source from already existing values in target.
   */
  const UPDATE_MODE_DEDUPLICATE_SOURCE = 'deduplicate-source';

  /**
   * The Configuration Layer ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Configuration Layer label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Configuration Layer description.
   *
   * @var string
   */
  protected $description = '';

  /**
   * The path to export to.
   *
   * @var string
   */
  protected $path = '';

  /**
   * The weight of the configuration layer.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The status, whether to be used by default.
   *
   * @var bool
   */
  protected $reset = FALSE;

  /**
   * The status, whether to be used by default.
   *
   * @var string
   */
  protected $source = self::CONFIG_SOURCE_DEFAULT;

  /**
   * The default update mode for this layer.
   *
   * @var string
   */
  protected $import_mode = self::UPDATE_MODE_REPLACE;

  /**
   * The default update mode for this layer.
   *
   * @var string
   */
  protected $export_mode = self::UPDATE_MODE_REPLACE;

  /**
   * The database storage.
   *
   * @var \Drupal\Core\Config\DatabaseStorage
   */
  protected $databaseStorage = NULL;

  /**
   * The file storage.
   *
   * @var \Drupal\Core\Config\FileStorage
   */
  protected $fileStorage = NULL;

  /**
   * Configuration is imported into layer when any of these events occur.
   *
   * @var array
   */
  protected $import_events = [];

  /**
   * Configuration is exported from layer when any of these events occur.
   *
   * @var array
   */
  protected $export_events = [];

  /**
   * Layer tags.
   *
   * @var array
   */
  protected $tags = [];

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description');
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    return $this->get('tags');
  }

  /**
   * {@inheritdoc}
   */
  public function setTags($tags) {
    $this->set('tags', $tags);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->get('path');
  }

  /**
   * {@inheritdoc}
   */
  public function setPath($path) {
    $this->set('path', $path);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight');
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReset() {
    return $this->get('reset');
  }

  /**
   * {@inheritdoc}
   */
  public function setReset($reset) {
    $this->set('reset', $reset);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSource() {
    return $this->get('source');
  }

  /**
   * {@inheritdoc}
   */
  public function setSource($source) {
    $this->set('source', $source);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExportMode() {
    return $this->get('export_mode');
  }

  /**
   * {@inheritdoc}
   */
  public function getImportMode() {
    return $this->get('import_mode');
  }

  /**
   * Events that will trigger a layer import.
   *
   * @return array
   *   The event names.
   */
  public function getImportEvents() {
    return $this->get('import_events');
  }

  /**
   * Events that will trigger a layer export.
   *
   * @return array
   *   The event names.
   */
  public function getExportEvents() {
    return $this->get('export_events');
  }

  /**
   * Name of database table.
   *
   * @return string
   *   The table name.
   */
  protected function getTableName() {
    return 'config_layer_' . $this->id;
  }

  /**
   * Get the database storage associated with this layer.
   *
   * @return \Drupal\Core\Config\DatabaseStorage
   *   The database storage.
   */
  public function getDatabaseStorage() {
    if (!isset($this->databaseStorage)) {
      $this->databaseStorage = new DatabaseStorage(\Drupal::database(), $this->getTableName());
    }
    return $this->databaseStorage;
  }

  /**
   * Get the file storage associated with this layer.
   *
   * @return \Drupal\Core\Config\FileStorage
   *   The file storage.
   */
  public function getFileStorage() {
    if (!isset($this->fileStorage)) {
      $this->fileStorage = new FileStorage($this->path);
    }
    return $this->fileStorage;
  }

  /**
   * Delete config layer entity and associated database table (if exists).
   */
  public function delete() {
    \Drupal::database()->schema()->dropTable($this->getTableName());
    parent::delete();
  }

  /**
   * The source storage used for importing.
   *
   * @return \Drupal\Core\Config\StorageInterface
   *   The source storage.
   */
  public function getSourceStorage() {
    switch ($this->source) {
      case self::CONFIG_SOURCE_DEFAULT:
        return $this->getFileStorage();

      case self::CONFIG_SOURCE_ACTIVE:
        return \Drupal::service('config.storage');
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function import($source_storage = NULL, $import_mode = NULL) {
    if (!isset($source_storage)) {
      $source_storage = $this->getSourceStorage();
    }
    if (!isset($import_mode)) {
      $import_mode = $this->import_mode;
    }
    ConfigLayerManager::combine($source_storage, $this->getDatabaseStorage(), $import_mode);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function export($target_storage = NULL, $export_mode = NULL) {
    if (!isset($target_storage)) {
      $target_storage = $this->getFileStorage();
    }
    if (!isset($export_mode)) {
      $export_mode = $this->export_mode;
    }
    ConfigLayerManager::combine($this->getDatabaseStorage(), $target_storage, $export_mode);
    return $this;
  }

}
